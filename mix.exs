defmodule R2b2.MixProject do
  use Mix.Project

  @version "0.1.0"

  @description """
  Easy integration into Backblaze B2 APIs
  """

  def project do
    [
      app: :r2b2,
      version: @version,
      elixir: "~> 1.11",
      elixirc_paths: ["lib", "test/support"],
      start_permanent: Mix.env() == :prod,
      description: @description,
      deps: deps(),
      dialyzer: [plt_add_apps: [:ecto]]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:ecto, "~> 2.0 or ~> 3.0", optional: true},
      {:hackney, "~> 1.17"},
      {:jason, "~> 1.2"}
    ]
  end
end
