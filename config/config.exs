use Mix.Config

config :r2b2,
  hackney_opts: System.get_env()["R2B2_HACKNEY_OPTS"],
  local_time_zone: System.get_env()["R2B2_LOCAL_TIME_ZONE"],
  application_key: System.get_env()["B2_APPLICATION_KEY"],
  key_id: System.get_env()["B2_KEY_ID"],
  bucket_id: System.get_env()["B2_BUCKET_ID"],
  bucket_name: System.get_env()["B2_BUCKET_NAME"],
  public_application_key: System.get_env()["B2_PUBLIC_APPLICATION_KEY"],
  public_key_id: System.get_env()["B2_PUBLIC_KEY_ID"],
  public_bucket_id: System.get_env()["B2_PUBLIC_BUCKET_ID"],
  public_bucket_name: System.get_env()["B2_PUBLIC_BUCKET_NAME"]
