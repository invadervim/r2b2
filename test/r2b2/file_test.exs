defmodule R2B2.FileTest do
  use ExUnit.Case, async: true

  alias Ecto.Changeset
  alias R2B2.File
  alias R2B2.API.RemoteFile

  @remote_file %RemoteFile{id: "UUID", file_name: "some/image.png"}
  @map %{id: "UUID", file_name: "some/image.png"}

  defmodule Schema do
    use Ecto.Schema

    schema "documents" do
      field(:file, File)
      field(:another_file, File, load: :db)
    end
  end

  test "schema" do
    assert Schema.__schema__(:type, :file) ==
             {:parameterized, File, [field: :file, schema: __MODULE__.Schema]}

    assert Schema.__schema__(:type, :another_file) ==
             {:parameterized, File, [load: :db, field: :another_file, schema: __MODULE__.Schema]}
  end

  describe "cast" do
    test "for RemoteFile" do
      cs = Changeset.cast(%Schema{}, %{file: @remote_file}, [:file])
      assert cs.errors == []

      assert {:ok, %RemoteFile{}} = File.cast(@remote_file, nil)
    end

    test "for map" do
      cs = Changeset.cast(%Schema{}, %{file: @map}, [:file])
      assert cs.errors == []

      assert {:ok, %RemoteFile{}} = File.cast(@map, nil)
    end

    test "for anything else" do
      assert :error = File.cast([id: "uuid"], nil)
    end
  end

  test "dump and load" do
    assert {:ok, %{"id" => _, "file_name" => _} = map} = File.dump(@remote_file, nil, [])
    assert {:ok, %RemoteFile{}} = File.load(map, nil, load: :db)
  end
end
