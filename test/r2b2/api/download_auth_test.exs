defmodule R2B2.API.DownloadAuthTest do
  use ExUnit.Case, async: true

  alias R2B2.API.{Response, DownloadAuth}
  alias R2B2.Fixtures

  @tag :direct_endpoint
  test "get upload url" do
    assert {:ok, %Response{status_code: 200} = response} =
             result =
             Fixtures.account_auth()
             |> DownloadAuth.get_download_authorization(Fixtures.bucket_id())

    assert {:ok, %DownloadAuth{token: token}} = DownloadAuth.create(response)
    assert {:ok, %DownloadAuth{}} = DownloadAuth.create(result)
    assert token
  end
end
