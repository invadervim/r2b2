defmodule R2B2.API.UploadTest do
  use ExUnit.Case, async: true

  alias R2B2.API.{RemoteFile, Upload}
  alias R2B2.Fixtures

  @tag :direct_endpoint
  test "upload file" do
    assert {:ok, %RemoteFile{} = uploaded_file} =
             Fixtures.upload_auth()
             |> Upload.upload_file(Fixtures.local_file())
             |> RemoteFile.create()

    # Clean up
    R2B2.API.Delete.delete_file_version(Fixtures.account_auth(), uploaded_file)
  end
end
