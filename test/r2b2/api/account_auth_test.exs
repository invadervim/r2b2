defmodule R2B2.API.AccountAuthTest do
  use ExUnit.Case, async: true

  alias R2B2.API.{AccountAuth, Response}
  alias R2B2.Fixtures

  @tag :direct_endpoint
  test "authorize account" do
    assert {:ok, %Response{status_code: 200} = response} =
             result =
             {Fixtures.key_id(), Fixtures.application_key()}
             |> AccountAuth.authorize_account()

    assert {:ok, %AccountAuth{}} = AccountAuth.create(response)
    assert {:ok, %AccountAuth{}} = AccountAuth.create(result)
  end
end
