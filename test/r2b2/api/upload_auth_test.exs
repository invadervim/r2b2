defmodule R2B2.API.UploadAuthTest do
  use ExUnit.Case, async: true

  alias R2B2.API.{Response, UploadAuth}
  alias R2B2.Fixtures

  @tag :direct_endpoint
  test "get upload url" do
    assert {:ok, %Response{} = response} =
             result = Fixtures.account_auth() |> UploadAuth.get_upload_url(Fixtures.bucket_id())

    assert {:ok, %UploadAuth{}} = UploadAuth.create(response)
    assert {:ok, %UploadAuth{}} = UploadAuth.create(result)
  end
end
