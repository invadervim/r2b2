defmodule R2B2.API.DeleteTest do
  use ExUnit.Case, async: true

  alias R2B2.API.{Delete, Response}
  alias R2B2.Fixtures

  setup do
    {:ok, account_auth: Fixtures.account_auth()}
  end

  setup do
    {:ok, uploaded_file: Fixtures.uploaded_file()}
  end

  @tag :direct_endpoint
  test "delete by file", %{account_auth: auth, uploaded_file: file} do
    assert {:ok, %Response{status_code: 200}} = Delete.delete_file_version(auth, file)

    # Should already be deleted
    assert {:ok, %Response{status_code: 400}} = Delete.delete_file_version(auth, file)
  end

  @tag :direct_endpoint
  test "delete by ID", %{account_auth: auth, uploaded_file: file} do
    assert {:ok, %Response{status_code: 200}} = Delete.delete_file_version(auth, file.id)
  end

  @tag :direct_endpoint
  test "delete by name", %{account_auth: auth, uploaded_file: file} do
    assert {:ok, %Response{status_code: 200}} =
             Delete.delete_file_version(auth, file.bucket_id, file.file_name)
  end
end
