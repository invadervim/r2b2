defmodule R2B2.API.RemoteFileTest do
  use ExUnit.Case, async: true

  alias R2B2.API.{Delete, RemoteFile, Response}
  alias R2B2.Fixtures

  setup do
    uploaded_file = Fixtures.uploaded_file()

    on_exit(fn ->
      R2B2.API.Delete.delete_file_version(Fixtures.account_auth(), uploaded_file)
    end)

    {:ok, uploaded_file: uploaded_file}
  end

  @tag :direct_endpoint
  describe "copy_file/4" do
    setup do: %{new_file_name: "avatars/user.png"}

    test "with source file and destination file", %{
      new_file_name: new_file_name,
      uploaded_file: source_file
    } do
      assert {:ok, %Response{status_code: 200}} =
               result =
               Fixtures.account_auth()
               |> RemoteFile.copy_file(source_file, %RemoteFile{file_name: new_file_name})

      assert {:ok, %RemoteFile{file_name: ^new_file_name}} = RemoteFile.create(result)
    end

    test "with source id and destination file_name", %{
      new_file_name: new_file_name,
      uploaded_file: source_file
    } do
      assert {:ok, %Response{status_code: 200}} =
               result =
               Fixtures.account_auth()
               |> RemoteFile.copy_file(source_file.id, new_file_name)

      assert {:ok, %RemoteFile{file_name: ^new_file_name}} = RemoteFile.create(result)
    end
  end

  @tag :direct_endpoint
  test "get file info" do
    file = Fixtures.uploaded_file()

    assert {:ok, %Response{status_code: 200}} =
             result =
             Fixtures.account_auth()
             |> RemoteFile.get_file_info(file)

    assert {:ok, %RemoteFile{} = remote_file} = RemoteFile.create(result)
    assert remote_file.name != remote_file.file_name

    # Clean up
    assert {:ok, %Response{status_code: 200}} =
             Fixtures.account_auth()
             |> Delete.delete_file_version(file)

    # Ensure it's gone
    assert {:ok, %Response{status_code: 404}} =
             Fixtures.account_auth()
             |> RemoteFile.get_file_info(file)
  end

  @tag :direct_endpoint
  test "get file info by name", %{uploaded_file: uploaded_file} do
    assert {:ok, %Response{status_code: 200} = response} =
             Fixtures.account_auth()
             |> RemoteFile.get_file_info_by_name(uploaded_file.bucket_id, uploaded_file.file_name)

    assert %RemoteFile{} = response.body
  end

  @tag :direct_endpoint
  test "list file names", %{uploaded_file: uploaded_file} do
    assert {:ok, %Response{status_code: 200} = response} =
             Fixtures.account_auth()
             |> RemoteFile.list_file_names(uploaded_file.bucket_id)

    assert response.body.files
           |> Enum.filter(&(&1.file_name == uploaded_file.file_name))
           |> length() == 1

    assert %RemoteFile{} = List.first(response.body.files)
  end

  @tag :direct_endpoint
  test "list file versions", %{uploaded_file: uploaded_file} do
    assert {:ok, %Response{status_code: 200} = response} =
             Fixtures.account_auth()
             |> RemoteFile.list_file_versions(uploaded_file.bucket_id)

    assert response.body.files
           |> Enum.filter(&(&1["fileName"] == uploaded_file.file_name))
           |> length() == 1
  end
end
