defmodule R2B2.API.DownloadTest do
  use ExUnit.Case, async: true

  alias R2B2.API.{Download, RemoteFile, Response}
  alias R2B2.{Fixtures, Util}

  setup do
    uploaded_file = Fixtures.uploaded_file()

    on_exit(fn ->
      R2B2.API.Delete.delete_file_version(Fixtures.account_auth(), uploaded_file)
    end)

    {:ok, uploaded_file: uploaded_file}
  end

  @tag :direct_endpoint
  test "download file by ID", %{uploaded_file: uploaded_file} do
    assert {:ok, %Response{request: request, status_code: 200}} =
             result =
             Fixtures.account_auth()
             |> Download.download_file_by_id(uploaded_file.id, options())

    assert Util.header(request.headers, "b2ContentType")
    refute Util.header(request.headers, "INVALID")

    assert {:ok, %RemoteFile{}} = RemoteFile.create(result)
  end

  @tag :direct_endpoint
  test "download file by name", %{uploaded_file: uploaded_file} do
    assert {:ok, %Response{request: request, status_code: 200}} =
             result =
             Fixtures.account_auth()
             |> Download.download_file_by_name(
               Fixtures.bucket_name(),
               uploaded_file.file_name,
               options()
             )

    assert "?b2ContentLanguage=en&b2ContentType=image/png" ==
             request.url
             |> String.split(".png")
             |> List.last()

    assert {:ok, %RemoteFile{}} = RemoteFile.create(result)
  end

  defp options(), do: %{content_language: "en", content_type: "image/png"}
end
