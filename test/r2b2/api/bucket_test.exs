defmodule R2B2.API.BucketTest do
  use ExUnit.Case, async: true

  alias R2B2.API.{Bucket, Response}
  alias R2B2.Fixtures

  describe "list" do
    @tag :direct_endpoint
    test "all buckets" do
      assert {:ok, %Response{status_code: 200}} = Bucket.list(Fixtures.account_auth())
    end

    @tag :direct_endpoint
    test "bucket id" do
      auth = Fixtures.account_auth()

      assert {:ok, %Response{status_code: 200}} =
               Bucket.list(auth, bucket_id: auth.allowed.bucket_id)

      assert {:ok, %Response{status_code: 400}} = Bucket.list(auth, bucket_id: "INVALID_ID")
    end

    @tag :direct_endpoint
    test "bucket name" do
      auth = Fixtures.account_auth()

      assert {:ok, %Response{status_code: 200}} =
               Bucket.list(auth, bucket_name: auth.allowed.bucket_name)

      assert {:ok, %Response{status_code: 401}} = Bucket.list(auth, bucket_name: "INVALID_NAME")
    end

    @tag :direct_endpoint
    test "bucket types" do
      auth = Fixtures.account_auth()
      assert {:ok, %Response{status_code: 200}} = Bucket.list(auth, bucket_types: ["allPrivate"])
      assert {:ok, %Response{status_code: 401}} = Bucket.list(auth, bucket_types: ["allPublic"])
      assert {:ok, %Response{status_code: 400}} = Bucket.list(auth, bucket_types: ["INVALID"])
    end
  end
end
