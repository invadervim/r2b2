defmodule R2B2.ClientTest do
  use ExUnit.Case, async: true

  alias R2B2.{Client, Fixtures}
  alias R2B2.API.{RemoteFile, UploadAuth}

  describe "private" do
    test "upload, download, and delete a file" do
      config_name = R2B2.TestClient
      assert {:ok, _client} = R2B2.Client.Config.start_link(name: config_name)

      assert {:ok, %RemoteFile{} = file} = Client.upload(config_name, Fixtures.local_file())
      assert {:ok, %RemoteFile{}} = Client.info(config_name, file)

      assert {:ok, %RemoteFile{link: link}} = Client.download_link(config_name, file)
      assert String.match?(link, ~r/elixir.png/)
      assert String.match?(link, ~r/\?Authorization/)

      assert {:ok, %RemoteFile{}} = Client.download(config_name, file)
      assert {:ok, %{status_code: 200}} = Client.delete(config_name, file)
    end
  end

  describe "public" do
    test "upload, download, and delete a file" do
      config_name = R2B2.TestClient

      assert {:ok, _client} =
               R2B2.Client.Config.start_link(
                 name: config_name,
                 application_key: Application.fetch_env!(:r2b2, :public_application_key),
                 key_id: Application.fetch_env!(:r2b2, :public_key_id),
                 bucket_id: Application.fetch_env!(:r2b2, :public_bucket_id),
                 bucket_name: Application.fetch_env!(:r2b2, :public_bucket_name)
               )

      assert {:ok, %UploadAuth{}} = Client.upload_auth(config_name)

      assert {:ok, %RemoteFile{} = file} = Client.upload(config_name, Fixtures.local_file())
      assert {:ok, %RemoteFile{} = info} = Client.info(config_name, file)
      assert info.content_disposition =~ info.name
      refute info.file_name =~ info.name

      new_name = "newname.png"
      new_file_name = "avatars/user.png"

      assert {:ok, %RemoteFile{} = copied_file} =
               Client.copy(config_name, file, new_file_name,
                 metadata_directive: "REPLACE",
                 content_type: file.content_type,
                 file_info: %{"b2-content-disposition": "attachment;filename=#{new_name}"}
               )

      assert copied_file.file_name == new_file_name
      assert copied_file.name == new_name

      assert {:ok, %RemoteFile{link: link}} = Client.download_link(config_name, file)
      assert String.match?(link, ~r/elixir.png/)
      refute String.match?(link, ~r/\?Authorization/)

      assert {:ok, %RemoteFile{}} = Client.download(config_name, file)
      assert {:ok, %{status_code: 200}} = Client.delete(config_name, file)
      assert {:ok, %{status_code: 200}} = Client.delete(config_name, copied_file)
    end
  end
end
