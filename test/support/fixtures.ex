defmodule R2B2.Fixtures do
  @moduledoc """
  Quickly create objects for testing.
  """

  alias R2B2.API.{LocalFile, RemoteFile}

  def account_auth do
    alias R2B2.API.AccountAuth

    {:ok, account_auth} =
      {key_id(), application_key()}
      |> AccountAuth.authorize_account()
      |> AccountAuth.create()

    account_auth
  end

  def application_key(), do: Application.fetch_env!(:r2b2, :application_key)

  def bucket_id(), do: Application.fetch_env!(:r2b2, :bucket_id)

  def bucket_name(), do: Application.fetch_env!(:r2b2, :bucket_name)

  def file_name, do: "dummy.png"

  def key_id(), do: Application.fetch_env!(:r2b2, :key_id)

  def local_path, do: "test/support/files/#{file_name()}"

  def local_file(path \\ nil) do
    {:ok, %LocalFile{} = file} = LocalFile.create(path || local_path(), "upload/elixir.png")
    file
  end

  def upload_auth(account_auth \\ account_auth()) do
    alias R2B2.API.UploadAuth

    {:ok, upload_auth} =
      account_auth
      |> UploadAuth.get_upload_url(bucket_id())
      |> UploadAuth.create()

    upload_auth
  end

  def uploaded_file() do
    {:ok, %RemoteFile{} = file} =
      upload_auth()
      |> R2B2.API.Upload.upload_file(local_file())
      |> RemoteFile.create()

    file
  end
end
