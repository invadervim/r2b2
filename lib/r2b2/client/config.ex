defmodule R2B2.Client.Config do
  @moduledoc """
  Stores authorization and other information.
  """

  require Logger

  use GenServer

  alias R2B2.API.AccountAuth

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: Keyword.get(opts, :name, __MODULE__))
  end

  @impl true
  def init(opts) do
    if Keyword.get(opts, :authorize_on_start, true), do: send(self(), :authorize)

    {:ok,
     %{
       app_key:
         Keyword.get(opts, :application_key, Application.fetch_env!(:r2b2, :application_key)),
       bucket_id: Keyword.get(opts, :bucket_id, Application.fetch_env!(:r2b2, :bucket_id)),
       bucket_name: Keyword.get(opts, :bucket_name, Application.fetch_env!(:r2b2, :bucket_name)),
       key_id: Keyword.get(opts, :key_id, Application.fetch_env!(:r2b2, :key_id)),
       account_auth: nil,
       bucket_type: nil,
       next_auth_pid: nil
     }}
  end

  @impl true
  def handle_info(:authorize, opts) do
    pid = self()
    spawn_link(fn -> GenServer.call(pid, :authorize) end)
    {:noreply, opts}
  end

  @impl true
  def handle_call(:authorize, _from, opts) do
    {:reply, :ok, new_auth(opts)}
  end

  @impl true
  def handle_call({:get, key}, _from, opts) do
    {:reply, Map.get(opts, key), opts}
  end

  defp new_auth(opts) do
    case authorize_account(opts) do
      {:ok, %AccountAuth{} = account_auth} ->
        Logger.info("Successfully retrieved B2 account authorization.")

        opts
        |> Map.delete(:errors)
        |> Map.merge(%{
          account_auth: account_auth,
          bucket_type: bucket_type(account_auth, opts.bucket_id),
          next_auth_pid: queue_authorization()
        })

      {:error, error_msg} ->
        Logger.error(error_msg)
        Map.put(opts, :errors, error_msg)
    end
  end

  defp authorize_account(opts) do
    {opts.key_id, opts.app_key}
    |> AccountAuth.authorize_account()
    |> AccountAuth.create()
  end

  defp bucket_type(account_auth, bucket_id) do
    {:ok, response} = R2B2.API.Bucket.list(account_auth, %{bucket_id: bucket_id})

    response.body.buckets
    |> List.first()
    |> Map.get("bucketType")
    |> String.slice(3..-1)
    |> String.downcase()
    |> String.to_atom()
  end

  # Backblaze token expires after 24 hours so we terminate the process to force a new server to
  # start and thus, grabbing a new token.
  defp queue_authorization() do
    Process.send_after(self(), :authorize, 24 * 60 * 60 * 1000)
  end
end
