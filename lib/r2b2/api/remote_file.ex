defmodule R2B2.API.RemoteFile do
  @moduledoc """
  Represents a file on B2's servers.

  ## Fields

  * `account_id`: The account where the file is stored.
  * `bucket_id`: The ID of the bucket where the file is stored.
  * `bucket_name`: The name of the bucket where the file is stored. This will not be set
    automatically as it would take another call.
  * `content_disposition`
  * `content_type`
  * `file`: The file itself.
  * `file_name`: The name of the file on B2's server, e.g. "uploads/elixir.png".
  * `id`: The file ID.
  * `link`: A link to download the file.
  * `md5`: Hash in md5 format. Useful for verify the file downloaded properly.
  * `name`: The name of the file, e.g. "elixir.png". This may not always match what's in
    `file_name`. For instance, you may want the `file_name` to be the UUID of an object in your
    app, while `name` would then be what your users see when downloading.
  * `sha1`: Hash in sha1 format. Useful for verify the file downloaded properly.
  * `size`
  * `uploaded_at`
  """

  use R2B2.Types

  alias R2B2.API.{AccountAuth, Request, Response}
  alias R2B2.Util

  @type t :: %__MODULE__{}

  @copy_file_endpoint "b2_copy_file"
  @get_file_info_endpoint "b2_get_file_info"
  @list_file_names_endpoint "b2_list_file_names"

  defstruct [
    :account_id,
    :bucket_id,
    :bucket_name,
    :content_disposition,
    :content_type,
    :file,
    :file_name,
    :id,
    :link,
    :md5,
    :name,
    :sha1,
    :size,
    :uploaded_at
  ]

  @doc """
  Returns a `%__MODULE__{}` from a successful response.
  """
  @spec create(response_or_error) :: remote_file_or_error
  def create({:ok, %Response{} = response}), do: create(response)
  def create({:error, _} = error), do: error

  @spec create(response) :: remote_file_or_error
  def create(%Response{body: body, status_code: 200}) when is_map(body),
    do: {:ok, do_create(body)}

  def create(%Response{body: %__MODULE__{}} = response), do: {:ok, response}

  # Used when downloading a file, as the body contains the file content and
  # the headers has the meta data.
  def create(%Response{body: [], status_code: 200} = response), do: {:ok, response}

  # Used when downloading a file, as the body contains the file content and
  # the headers has the meta data.
  def create(%Response{body: body, headers: headers, status_code: 200}) do
    remote_file =
      %{
        content_length: Util.header(headers, "Content-Length"),
        content_sha1: Util.header(headers, "x-bz-content-sha1"),
        content_type: Util.header(headers, "Content-Type"),
        file: body,
        file_id: Util.header(headers, "x-bz-file-id"),
        file_info: %{"b2-content-disposition" => Util.header(headers, "Content-Disposition")},
        file_name: Util.header(headers, "x-bz-file-name"),
        upload_timestamp: uploaded_timestamp(headers)
      }
      |> do_create()

    {:ok, remote_file}
  end

  @spec create(files :: list) :: {:ok, [__MODULE__.t()]} | {:error, any}
  def create(files) when is_list(files) do
    files = Enum.map(files, &(&1 |> Util.atomize_keys() |> do_create()))
    {:ok, files}
  end

  def create(files), do: {:error, files}

  @spec do_create(body :: map) :: %__MODULE__{}
  defp do_create(%__MODULE__{} = body), do: body

  defp do_create(body) do
    {:ok, dt} = DateTime.from_unix(body.upload_timestamp, :millisecond)
    content_disposition = body.file_info["b2-content-disposition"]

    %__MODULE__{
      account_id: Map.get(body, :account_id),
      bucket_id: Map.get(body, :bucket_id),
      content_disposition: content_disposition,
      content_type: body.content_type,
      file: Map.get(body, :file),
      file_name: body.file_name,
      id: body.file_id,
      md5: Map.get(body, :content_md5),
      name: content_disposition_name(content_disposition) || Path.basename(body.file_name),
      sha1: Map.get(body, :content_sha1),
      size: body.content_length,
      uploaded_at: dt
    }
  end

  defp content_disposition_name(content_disposition) do
    strs = String.split(content_disposition, "filename=")
    if length(strs) > 1, do: List.last(strs)
  end

  @doc """
  Copies a file to a new location.

  Options:
  * `content_type`
  * `destination_bucket_id`
  * `destination_server_side_encryption`
  * `file_info`
  * `metadata_directive`
  * `range`
  * `sourceServerSideEncryption`
  """
  @spec copy_file(
          account_auth,
          source_file_or_id :: __MODULE__.t() | String.t(),
          destination_file_or_id :: __MODULE__.t() | String.t()
        ) :: response_or_error

  @spec copy_file(
          account_auth,
          source_file_or_id :: __MODULE__.t() | String.t(),
          destination_file_or_id :: __MODULE__.t() | String.t(),
          options
        ) :: response_or_error
  def copy_file(account_auth, source_file_or_id, destination_file_or_file_name, options \\ %{})

  def copy_file(account_auth, %__MODULE__{id: id}, destination, options) do
    copy_file(account_auth, id, destination, options)
  end

  def copy_file(account_auth, source_file_id, %__MODULE__{file_name: file_name}, options) do
    copy_file(account_auth, source_file_id, file_name, options)
  end

  def copy_file(%AccountAuth{} = account_auth, source_file_id, destination_file_name, options) do
    %Request{
      auth: account_auth,
      body: copy_file_attrs(source_file_id, destination_file_name, options),
      endpoint: @copy_file_endpoint
    }
    |> Request.send()
  end

  defp copy_file_attrs(id, file_name, opts) do
    opts
    |> Util.stringify_keys(false)
    |> Map.merge(%{sourceFileId: id, fileName: file_name})
    |> Jason.encode!()
  end

  @doc """
  Grab file info.
  """
  @spec get_file_info(account_auth, __MODULE__.t()) :: response_or_error
  def get_file_info(account_auth, %__MODULE__{id: file_id}) do
    get_file_info(account_auth, file_id)
  end

  @spec get_file_info(account_auth, file_id) :: response_or_error
  def get_file_info(%AccountAuth{} = account_auth, file_id) do
    %Request{
      auth: account_auth,
      body: Jason.encode!(%{fileId: file_id}),
      endpoint: @get_file_info_endpoint
    }
    |> Request.send()
    |> case do
      {:ok, %Response{status_code: 200} = response} ->
        file = do_create(response.body)
        {:ok, Map.put(response, :body, file)}

      error ->
        error
    end
  end

  @doc """
  Grab file info using the name of the file. If multiple files are found, will only return the
  first file.
  """
  @spec get_file_info_by_name(account_auth, bucket_id, file_name_prefix :: String.t()) ::
          response_or_error
  def get_file_info_by_name(%AccountAuth{} = account_auth, bucket_id, file_name_prefix) do
    account_auth
    |> list_file_names(bucket_id, %{prefix: file_name_prefix})
    |> case do
      {:ok, %Response{body: %{files: files}} = response} ->
        if !Enum.empty?(files),
          do: {:ok, Map.put(response, :body, hd(files))},
          else: {:error, response}

      error ->
        error
    end
  end

  @doc """
  List file names.

  Options:
  * `delimiter`
  * `max_file_count`
  * `prefix`
  * `start_file_name`
  """
  @spec list_file_names(account_auth, bucket_id) :: response_or_error
  @spec list_file_names(account_auth, bucket_id, options) :: response_or_error
  def list_file_names(%AccountAuth{} = account_auth, bucket_id, options \\ %{}) do
    %Request{
      auth: account_auth,
      body: list_file_body(bucket_id, options),
      endpoint: @list_file_names_endpoint
    }
    |> Request.send()
    |> case do
      {:ok, %Response{status_code: 200} = response} ->
        {:ok, files} = create(response.body.files)
        {:ok, put_in(response, [Access.key!(:body), Access.key!(:files)], files)}

      error ->
        error
    end
  end

  @doc """
  List file versions.

  Options:
  * `delimiter`
  * `max_file_count`
  * `prefix`
  * `start_file_id`
  * `start_file_name`
  """
  @spec list_file_versions(account_auth, bucket_id) :: response_or_error
  @spec list_file_versions(account_auth, bucket_id, options) :: response_or_error
  def list_file_versions(%AccountAuth{} = account_auth, bucket_id, options \\ %{}) do
    %Request{
      auth: account_auth,
      body: list_file_body(bucket_id, options),
      endpoint: @list_file_names_endpoint
    }
    |> Request.send()
  end

  defp list_file_body(bucket_id, options) do
    options
    |> Map.put(:bucket_id, bucket_id)
    |> Util.stringify_keys(false)
    |> Jason.encode!()
  end

  defp uploaded_timestamp(headers) do
    {ms, _} =
      headers
      |> Util.header("X-Bz-Upload-Timestamp")
      |> Integer.parse()

    ms
  end
end
