defmodule R2B2.API.Bucket do
  @moduledoc """
  Dealing with all things bucket-wise.
  """

  use R2B2.Types

  alias R2B2.API.{AccountAuth, Request}

  @list_endpoint "b2_list_buckets"

  @doc """
  Show all buckets.

  Options:
  * `bucket_id`
  * `bucket_name`
  * `bucket_types`: a list containing `allPublic`, `allPrivate`, `snapshot`, or `all`.
  """
  @spec list(AccountAuth.t()) :: response_or_error
  @spec list(AccountAuth.t(), options) :: response_or_error
  def list(%AccountAuth{} = auth, options \\ %{}) do
    %Request{
      auth: auth,
      body: list_body(auth, options),
      endpoint: @list_endpoint
    }
    |> Request.send()
  end

  defp list_body(auth, options) do
    %{accountId: auth.account_id}
    |> Map.merge(additional_options(options))
    |> Jason.encode!()
  end

  defp additional_options(options) do
    for {key, val} <- options,
        into: %{},
        do: {stringify(key), val}
  end

  defp stringify(key) do
    key
    |> Atom.to_string()
    |> Macro.camelize()
    |> lowercase()
  end

  defp lowercase(key) do
    with <<c::utf8, rest::binary>> <- key do
      String.downcase(<<c>>) <> rest
    end
  end
end
