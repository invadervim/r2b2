defmodule R2B2.API.AccountAuth do
  @moduledoc """
  Grants access to use most B2 endpoints.
  """

  use R2B2.Types

  alias R2B2.API.{Request, Response}

  @type t :: %__MODULE__{}

  @authorize_endpoint "b2_authorize_account"

  defstruct [
    :absolute_min_part_size,
    :account_id,
    :allowed,
    :api_url,
    :download_url,
    :recommended_part_size,
    :token
  ]

  @doc """
  Returns a `%__MODULE__{}` from a successful response.
  """
  @spec create(response_or_error) :: {:ok, account_auth} | {:error, response}
  def create({:ok, %Response{} = response}), do: create(response)
  def create({:error, _} = error), do: error

  @spec create(response) :: {:ok, account_auth} | {:error, response}
  def create(%Response{status_code: 200, body: body}) do
    {:ok, struct(__MODULE__, get_values(body))}
  end

  def create(response), do: {:error, response}

  @doc """
  Sends an account authorization request.
  """
  @spec authorize_account({key_id :: String.t(), application_key :: String.t()}) ::
          response_or_error
  def authorize_account({_key_id, _application_key} = auth) do
    %Request{
      endpoint: @authorize_endpoint,
      http_method: :get,
      options: [basic_auth: auth]
    }
    |> Request.send()
  end

  defp get_values(body) do
    body
    |> Map.put(:allowed, R2B2.Util.atomize_keys(body.allowed))
    |> Map.put(:token, body.authorization_token)
    |> Map.drop([:authorization_token])
  end
end
