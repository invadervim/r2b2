defmodule R2B2.API.Upload do
  @moduledoc """
  Uploads a file. To upload a large file (>5GB), see `R2B2.API.LargeUpload`.
  """

  use R2B2.Types

  alias R2B2.API.{LocalFile, Request, UploadAuth}

  @upload_file_endpoint "b2_upload_file"

  @doc """
  Upload a file.
  """
  @spec upload_file(UploadAuth.t(), local_file) :: response_or_error
  def upload_file(%UploadAuth{} = upload_auth, %LocalFile{} = local_file) do
    %Request{
      auth: upload_auth,
      body: local_file.file,
      endpoint: @upload_file_endpoint,
      headers: upload_headers(local_file),
      url: upload_auth.url
    }
    |> Request.send()
  end

  defp upload_headers(file) do
    [
      {"Content-Length", file.size},
      {"Content-Type", file.content_type},
      {"X-Bz-Content-Sha1", file.sha1},
      {"X-Bz-File-Name", file.file_name},
      {"X-Bz-Info-b2-content-disposition", file.content_disposition}
    ]
  end
end
