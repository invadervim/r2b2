defmodule R2B2.API.Delete do
  @moduledoc """
  Delete a file.
  """

  use R2B2.Types

  alias R2B2.API.{AccountAuth, RemoteFile, Request, Response}

  @delete_file_version_endpoint "b2_delete_file_version"

  @doc """
  Delete a file. This accepts either a `%Stardust.API.RemoteFile{}`, a `file_id`, or a
  `bucket_id` and `file_name`.
  """
  @spec delete_file_version(account_auth, remote_file) :: response_or_error
  def delete_file_version(%AccountAuth{} = account_auth, %RemoteFile{id: id, file_name: file_name}) do
    %Request{
      auth: account_auth,
      body: Jason.encode!(%{fileId: id, fileName: file_name}),
      endpoint: @delete_file_version_endpoint
    }
    |> Request.send()
  end

  @spec delete_file_version(account_auth, file_id :: String.t()) :: response_or_error
  def delete_file_version(%AccountAuth{} = account_auth, file_id) do
    {:ok, %Response{body: file}} = RemoteFile.get_file_info(account_auth, file_id)
    delete_file_version(account_auth, file)
  end

  @spec delete_file_version(account_auth, bucket_id :: String.t(), file_name :: String.t()) ::
          response_or_error
  def delete_file_version(%AccountAuth{} = account_auth, bucket_id, file_name) do
    {:ok, %Response{body: file}} =
      RemoteFile.get_file_info_by_name(account_auth, bucket_id, file_name)

    delete_file_version(account_auth, file)
  end
end
