defmodule R2B2.API.Download do
  @moduledoc """
  Downloads a file.
  """

  use R2B2.Types

  alias R2B2.API.{AccountAuth, Request}
  alias R2B2.Util

  @download_file_by_id_endpoint "b2_download_file_by_id"

  @doc """
  Download a specific version of a file.

  Options:
  * `cache_control`
  * `content_disposition`
  * `content_encoding`
  * `content_language`
  * `content_type`
  * `expires`
  * `range`
  """
  @spec download_file_by_id(account_auth, file_id) :: response_or_error
  @spec download_file_by_id(account_auth, file_id, options) :: response_or_error
  def download_file_by_id(%AccountAuth{} = account_auth, file_id, options \\ %{}) do
    %Request{
      auth: account_auth,
      body: Jason.encode!(%{fileId: file_id}),
      endpoint: @download_file_by_id_endpoint,
      headers: by_id_headers(options),
      host: account_auth.download_url
    }
    |> Request.send()
  end

  @doc """
  Download the latest version of a file. See `download_file_by_id` for options.
  """
  @spec download_file_by_name(account_auth, bucket_name, file_name, options) :: response_or_error
  def download_file_by_name(%AccountAuth{} = account_auth, bucket_name, file_name, options \\ %{}) do
    %Request{
      auth: account_auth,
      headers: by_name_headers(options),
      http_method: :get,
      url: download_file_url(account_auth, bucket_name, file_name, options)
    }
    |> Request.send()
  end

  defp by_id_headers(headers) do
    headers
    |> Util.stringify_keys()
    |> rename_range_key()
    |> Map.to_list()
  end

  defp by_name_headers(options) do
    case Map.get(options, :range) do
      nil -> []
      range -> [{"range", range}]
    end
  end

  defp by_name_url_params(options) when options == %{}, do: ""

  defp by_name_url_params(options) do
    params =
      options
      |> Map.drop([:range])
      |> Util.stringify_keys()

    for({key, val} <- params, into: "", do: "&#{key}=#{val}")
    |> String.replace_prefix("&", "?")
  end

  defp download_file_url(account_auth, bucket_name, file_name, options) do
    "#{account_auth.download_url}/file/#{bucket_name}/#{file_name}#{by_name_url_params(options)}"
  end

  defp rename_range_key(headers) do
    if Map.has_key?(headers, "b2Range") do
      headers
      |> Map.put("range", Map.get(headers, :range))
      |> Map.drop(["b2Range"])
    else
      headers
    end
  end
end
