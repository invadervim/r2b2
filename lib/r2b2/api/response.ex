defmodule R2B2.API.Response do
  @moduledoc """
  Representation of a response back from B2.
  """

  @type t :: %__MODULE__{}

  defstruct [:body, :headers, :request, :status_code]

  @doc """
  Returns a `%__MODULE__{}`.
  """
  @spec create({:ok | :error, integer, list(tuple), any}, R2B2.API.Request.t()) ::
          {:ok, __MODULE__.t()} | any
  def create({:ok, status_code, response_headers, client_ref}, request) do
    mime_type = mime_type(response_headers)
    {:ok, hackney_body} = :hackney.body(client_ref)
    {:ok, body} = format_body(hackney_body, mime_type)

    {
      :ok,
      struct(__MODULE__, %{
        body: body,
        headers: response_headers,
        request: request,
        status_code: status_code
      })
    }
  end

  def create(error, _request), do: error

  defp mime_type(headers) do
    headers
    |> R2B2.Util.header("Content-Type")
    |> String.split(";")
    |> List.first()
  end

  defp format_body(body, "application/json") do
    with {:ok, json} <- Jason.decode(body) do
      {:ok, R2B2.Util.atomize_keys(json)}
    end
  end

  defp format_body(body, _), do: {:ok, body}
end
