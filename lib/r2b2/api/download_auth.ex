defmodule R2B2.API.DownloadAuth do
  @moduledoc """
  Grants access to download a file.
  """

  use R2B2.Types

  alias R2B2.API.{AccountAuth, Request, Response}
  alias R2B2.Util

  @type t :: %__MODULE__{}

  @get_download_authorization_endpoint "b2_get_download_authorization"
  @three_days 259_200

  defstruct [:bucket_id, :prefix, :token]

  @doc """
  Creates an `__MODULE__` from a successful response.
  """
  @spec create(response_or_error) :: {:ok, %__MODULE__{}} | {:error, response}
  def create({:ok, %Response{} = response}), do: create(response)
  def create({:error, _} = error), do: error

  @spec create(response) :: {:ok, %__MODULE__{}} | {:error, response}
  def create(%Response{status_code: 200, body: body}) do
    {
      :ok,
      %__MODULE__{
        bucket_id: body.bucket_id,
        prefix: body.file_name_prefix,
        token: body.authorization_token
      }
    }
  end

  def create(response), do: {:error, response}

  @doc """
  Sends a download authorization request for private files.

  Options:
  * `bucket_id`: Only necessary if one is not set in configuration.
  * `cache_control`
  * `content_disposition`
  * `content_encoding`
  * `content_language`
  * `content_type`
  * `expires`
  * `prefix`: Defaults to "".
  * `valid_duration`: Determines how long the link will be valid. To use TimeZone, ensure that
    `config :elixir, :time_zone_database` has been set.

    Formats accepted:
    * `integer`: Time in seconds
    * `{%DateTime{}}`: Will expire on provided date, time, and provided time zone.
    * `{%Date{}}`: Will expire on provided date at 11:59:59pm of a time zone provided in
      `B2_LOCAL_TIME_ZONE` or UTC.
    * `{%Date{}, TimeZone}`: Same as above, but with provided time zone used instead.
    * `nil`: 3 days from now at 11:59:59pm UTC.
  """
  @spec get_download_authorization(account_auth, bucket_id) :: response_or_error
  @spec get_download_authorization(account_auth, bucket_id, options) :: response_or_error
  def get_download_authorization(%AccountAuth{} = account_auth, bucket_id, options \\ %{}) do
    %Request{
      auth: account_auth,
      body: download_auth_body(bucket_id, options),
      endpoint: @get_download_authorization_endpoint
    }
    |> Request.send()
  end

  defp download_auth_body(bucket_id, options) do
    options
    |> Util.stringify_keys()
    |> Map.merge(%{
      bucketId: bucket_id,
      fileNamePrefix: Map.get(options, :prefix, ""),
      validDurationInSeconds:
        options
        |> Map.get(:valid_duration)
        |> valid_duration()
    })
    |> Jason.encode!()
  end

  defp now() do
    {:ok, now} = time_zone() |> DateTime.now()
    now
  end

  defp time_zone(), do: Application.get_env(:r2b2, :local_time_zone) || "Etc/UTC"

  defp valid_duration(expires_in) when is_integer(expires_in), do: expires_in

  defp valid_duration(%DateTime{} = datetime) do
    now()
    |> DateTime.diff(datetime)
    |> abs()
  end

  defp valid_duration({%Date{} = date, time_zone}) do
    {:ok, dt} = DateTime.new(date, %Time{hour: 11, minute: 59, second: 59}, time_zone)
    valid_duration(dt)
  end

  defp valid_duration(%Date{} = expires_date) do
    valid_duration({expires_date, time_zone()})
  end

  defp valid_duration(nil) do
    %{now() | hour: 23, minute: 59, second: 59}
    |> DateTime.add(@three_days, :second)
    |> valid_duration()
  end
end
