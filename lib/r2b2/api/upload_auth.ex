defmodule R2B2.API.UploadAuth do
  @moduledoc """
  Grants access to upload a file.
  """

  use R2B2.Types

  alias R2B2.API.{AccountAuth, Request, Response}

  @type t :: %__MODULE__{}

  @get_upload_url_endpoint "b2_get_upload_url"

  defstruct [:bucket_id, :token, :url]

  @doc """
  Creates an `__MODULE__` from a successful response.
  """
  @spec create(response_or_error) :: {:ok, %__MODULE__{}} | {:error, response}
  def create({:ok, %Response{} = response}), do: create(response)
  def create({:error, _} = error), do: error

  @spec create(%Response{}) :: {:ok, %__MODULE__{}} | {:error, response}
  def create(%Response{status_code: 200, body: body}) do
    {
      :ok,
      %__MODULE__{
        bucket_id: body.bucket_id,
        token: body.authorization_token,
        url: body.upload_url
      }
    }
  end

  def create(response), do: {:error, response}

  @doc """
  Sends an upload authorization request.
  """
  @spec get_upload_url(account_auth, bucket_id) :: response_or_error
  def get_upload_url(%AccountAuth{} = account_auth, bucket_id) do
    %Request{
      auth: account_auth,
      body: Jason.encode!(%{bucketId: bucket_id}),
      endpoint: @get_upload_url_endpoint
    }
    |> Request.send()
  end
end
