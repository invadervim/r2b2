defmodule R2B2.API.Request do
  @moduledoc """
  Manages requests to B2 servers.
  """

  use R2B2.Types

  @type t :: %__MODULE__{}

  defstruct api_version: Application.get_env(:r2b2, :api_version, "v2"),
            auth: nil,
            body: "",
            endpoint: nil,
            headers: [],
            host: Application.get_env(:r2b2, :scheme, "https://api.backblazeb2.com"),
            http_method: :post,
            options: [],
            url: nil

  alias R2B2.API.Response

  @doc """
  Submits the request to B2. An error is only returned if the request could not be made, such as
  on timeout.
  """
  @spec send(__MODULE__.t()) :: response_or_error
  def send(%__MODULE__{auth: auth} = request) when is_nil(auth) do
    request_url = url(request)

    :hackney.request(
      request.http_method,
      request_url,
      request.headers,
      request.body,
      (Application.get_env(:r2b2, :hackney_opts) || []) ++ request.options
    )
    |> Response.create(%{request | url: request_url})
  end

  def send(%__MODULE__{auth: auth} = request) do
    request
    |> Map.merge(%{auth: nil, headers: auth_headers(request), host: auth_url(auth)})
    |> send()
  end

  @doc """
  Determines what the URL should be. If `url` is already defined, then use that. Otherwise, it'll
  be built from the other fields.
  """
  @spec url(%__MODULE__{}) :: String.t()
  def url(%__MODULE__{url: nil} = request) do
    "#{request.host}/b2api/#{request.api_version}/#{request.endpoint}"
  end

  def url(request), do: request.url

  defp auth_headers(%__MODULE__{auth: auth, headers: headers}) do
    [{"Authorization", auth.token} | headers]
  end

  defp auth_url(%{api_url: api_url}), do: api_url
  defp auth_url(_), do: nil
end
