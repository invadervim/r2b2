defmodule R2B2.API.LocalFile do
  @moduledoc """
  Represents a file on local servers. Used to collect information to upload a file.

  ## Options
  * `content_disposition`
  * `content_type`
  * `file_name`

  ## Fields

  * `content_disposition`
  * `content_type`
  * `file`: The file itself.
  * `file_name`: The name of the file on B2's server, e.g. "uploads/elixir.png".
  * `name`: The name of the file, e.g. "elixir.png". This may not always match what's in
    `file_name`. For instance, you may want the `file_name` to be the UUID of an object in your
    app, while `name` would then be what your users see when downloading.
  * `original_name`: The name of the file when created.
  * `sha1`: Hash in sha1 format. Useful for verify the file downloaded properly.
  * `size`
  """

  use R2B2.Types

  @type t :: %__MODULE__{}

  defstruct [
    :content_disposition,
    :content_type,
    :file,
    :file_name,
    :name,
    :original_name,
    :sha1,
    :size
  ]

  @doc """
  Returns a `%__MODULE__{}`. Should be used to grab file information from a B2 file.
  """
  @spec create(local_path :: String.t(), file_name) :: {:ok, local_file}
  @spec create(local_path :: String.t(), file_name, options) :: {:ok, local_file}
  def create(local_path, file_name, options \\ %{}) do
    {:ok, file} = File.read(local_path)
    original_name = Path.basename(local_path)
    name = Map.get(options, :name, original_name)
    {:ok, %{size: size}} = File.stat(local_path)
    sha1 = :crypto.hash(:sha, file) |> Base.encode16(case: :lower)

    {
      :ok,
      %__MODULE__{
        content_disposition:
          Map.get(options, :content_disposition, "attachment;filename=#{name}"),
        content_type: Map.get(options, :content_type, "b2/x-auto"),
        file: file,
        file_name: file_name,
        name: name,
        original_name: original_name,
        sha1: sha1,
        size: size
      }
    }
  end
end
