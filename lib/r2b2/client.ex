defmodule R2B2.Client do
  @moduledoc """
  Puts together the most common actions in one place.
  """

  require Logger

  use R2B2.Types

  alias R2B2.API.{
    Delete,
    Download,
    DownloadAuth,
    LocalFile,
    RemoteFile,
    Upload,
    UploadAuth
  }

  @typedoc """
  The name of the GenServer to use.
  """
  @type config_name :: atom

  @doc """
  Grabs the client's account authorization.
  """
  @spec account_auth(config_name) :: account_auth
  def account_auth(config_name), do: GenServer.call(config_name, {:get, :account_auth})

  @doc """
  Grabs the client's bucket ID.
  """
  @spec bucket_id(config_name) :: bucket_id
  def bucket_id(config_name), do: GenServer.call(config_name, {:get, :bucket_id})

  @doc """
  Grabs the client's bucket name.
  """
  @spec bucket_name(config_name) :: bucket_name
  def bucket_name(config_name), do: GenServer.call(config_name, {:get, :bucket_name})

  @doc """
  Grabs the client's bucket type (`:public` or `:private`).
  """
  @spec bucket_type(config_name) :: atom
  def bucket_type(config_name), do: GenServer.call(config_name, {:get, :bucket_type})

  @doc """
  Copies a file.
  """
  @spec copy(
          config_name,
          source_file_or_id :: remote_file | String.t(),
          destination_file_or_file_name :: remote_file | String.t()
        ) :: remote_file_or_error

  @spec copy(
          config_name,
          source_file_or_id :: remote_file | String.t(),
          destination_file_or_file_name :: remote_file | String.t(),
          options
        ) :: remote_file_or_error
  def copy(config_name, source_file_or_id, destination_file_or_file_name, options \\ %{}) do
    log_action("Copy", config_name)

    config_name
    |> account_auth()
    |> RemoteFile.copy_file(source_file_or_id, destination_file_or_file_name, options)
    |> RemoteFile.create()
  end

  @doc """
  Delete a file.
  """
  @spec delete(config_name, remote_file :: %RemoteFile{}) :: response_or_error
  def delete(config_name, %RemoteFile{} = remote_file) do
    log_action("Delete", config_name)

    config_name
    |> account_auth()
    |> Delete.delete_file_version(remote_file)
  end

  @doc """
  Download a file. The `%RemoteFile{}` must have `:id` or `:file_name` set.
  """
  @spec download(config_name, remote_file) :: remote_file_or_error
  @spec download(config_name, remote_file, options) :: remote_file_or_error
  def download(config_name, %RemoteFile{} = remote_file, options \\ %{}) do
    log_action("Download", config_name)

    case download_method(remote_file) do
      :id ->
        config_name
        |> account_auth()
        |> Download.download_file_by_id(remote_file.id, options)
        |> RemoteFile.create()

      :file_name ->
        config_name
        |> account_auth()
        |> Download.download_file_by_name(
          bucket_name(config_name),
          remote_file.file_name,
          options
        )
        |> RemoteFile.create()

      error ->
        error
    end
  end

  defp download_method(%RemoteFile{id: id}) when id != nil, do: :id
  defp download_method(%RemoteFile{file_name: file_name}) when file_name != nil, do: :file_name
  defp download_method(_), do: {:error, :no_file_id_or_file_name}

  @doc """
  Grabs a download link.
  """
  @spec download_link(config_name, remote_file) :: remote_file_or_error
  @spec download_link(config_name, remote_file, options) :: remote_file_or_error
  def download_link(config_name, %RemoteFile{} = remote_file, options \\ %{}) do
    log_action("Download Link", config_name)

    url = account_auth(config_name).download_url
    token = download_token(config_name, options)
    link = "#{url}/file/#{bucket_name(config_name)}/#{remote_file.file_name}#{token}"
    {:ok, Map.put(remote_file, :link, link)}
  end

  defp download_token(config_name, options) do
    config_name
    |> bucket_type()
    |> download_token(config_name, options)
  end

  defp download_token(:public, _, _), do: nil

  defp download_token(:private, config_name, options) do
    config_name
    |> account_auth()
    |> DownloadAuth.get_download_authorization(bucket_id(config_name), options)
    |> DownloadAuth.create()
    |> case do
      {:ok, %DownloadAuth{} = download_auth} -> "?Authorization=#{download_auth.token}"
      _error -> nil
    end
  end

  @doc """
  Grabs the metadata of a file.
  """
  @spec info(config_name, remote_file) :: remote_file_or_error
  def info(config_name, %RemoteFile{id: id} = remote_file) when is_binary(id) do
    log_action("Info (by file ID)", config_name)

    config_name
    |> account_auth()
    |> R2B2.API.RemoteFile.get_file_info(remote_file)
    |> RemoteFile.create()
  end

  def info(config_name, %RemoteFile{file_name: file_name}) when is_binary(file_name) do
    log_action("Info (by file name)", config_name)

    account_auth(config_name)
    |> R2B2.API.RemoteFile.get_file_info_by_name(bucket_id(config_name), file_name)
    |> RemoteFile.create()
  end

  @spec upload(config_name, local_file) :: remote_file_or_error
  def upload(config_name, %LocalFile{} = local_file) do
    log_action("Upload", config_name)

    with {:ok, upload_auth_response} <-
           UploadAuth.get_upload_url(account_auth(config_name), bucket_id(config_name)),
         {:ok, upload_auth} <- UploadAuth.create(upload_auth_response),
         {:ok, upload_response} <- Upload.upload_file(upload_auth, local_file) do
      RemoteFile.create(upload_response)
    else
      error -> error
    end
  end

  @spec upload_auth(config_name) :: remote_file_or_error
  def upload_auth(config_name) do
    log_action("Upload Auth", config_name)

    config_name
    |> account_auth()
    |> UploadAuth.get_upload_url(bucket_id(config_name))
    |> UploadAuth.create()
  end

  defdelegate child_spec(opts), to: R2B2.Client.Config

  defp log_action(action, config_name) do
    Logger.info("R2-B2 client #{action} for #{config_name}")
  end
end
