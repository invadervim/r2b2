defmodule R2B2.Util do
  @moduledoc """
  Shared functions.
  """

  def atomize(key) do
    key
    |> Macro.underscore()
    |> String.to_atom()
  end

  def atomize_keys(options) do
    for {key, val} <- options,
        into: %{},
        do: {atomize(key), val}
  end

  def header(headers, key) do
    case List.keyfind(headers, key, 0) do
      nil -> nil
      {_, value} -> value
    end
  end

  def lowercase(key) do
    with <<c::utf8, rest::binary>> <- key do
      String.downcase(<<c>>) <> rest
    end
  end

  def stringify(key, prefix_b2 \\ true) do
    key
    |> Atom.to_string()
    |> do_prefix_b2(prefix_b2)
    |> Macro.camelize()
    |> lowercase()
  end

  def do_prefix_b2(key, true), do: "b2_#{key}"
  def do_prefix_b2(key, false), do: key

  def stringify_keys(options, prefix_b2 \\ true) do
    for {key, val} <- options,
        into: %{},
        do: {stringify(key, prefix_b2), val}
  end
end
