defmodule R2B2.Types do
  @moduledoc """
  Common typespec types to be used throughout.
  """

  defmacro __using__(_opts) do
    quote location: :keep do
      @type account_auth :: R2B2.API.AccountAuth.t()
      @type bucket_id :: String.t()
      @type bucket_name :: String.t()

      @typedoc """
      The ID of the file on B2.
      """
      @type file_id :: String.t()

      @typedoc """
      The file name of the file on B2, e.g. "uploads/elixir.png".
      """
      @type file_name :: String.t()

      @type local_file :: R2B2.API.LocalFile.t()
      @type options :: map
      @type remote_file :: R2B2.API.RemoteFile.t()
      @type remote_file_or_error :: {:ok, remote_file} | {:error, response}
      @type response :: R2B2.API.Response.t()
      @type response_or_error :: {:ok, response} | {:error, response}
    end
  end
end
