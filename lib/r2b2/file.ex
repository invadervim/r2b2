defmodule R2B2.File do
  @moduledoc """
  A custom Ecto field type for saving metadata into your database.

  To use, simple add `R2B2.File` as your field's type:

  ```
  defmodule User do
    use Ecto.Schema

    schema "users" do
      field :avatar, R2B2.File
    end
  end
  ```

  This accepts an `R2B2.API.RemoteFile{}` or a map with similar keys and will convert it into a
  map in the database.

  # Options

  ## fields

  The default fields saved are `id`, `file_name`. Use the `fields` parameter. For example,
  to also save the content type, do:

  ```
  defmodule User do
    use Ecto.Schema

    schema "users" do
      field :avatar, R2B2.File, fields: [:content_type]
    end
  end
  ```

  Any key in `R2B2.API.RemoteFile` can be saved except for `file`. Note that `id` and `file_name`
  will always be saved.

  ## Load Method

  When retrieving data, a `get_file_info` call will automatically be made. However, you can chooose

  ```
  defmodule User do
    use Ecto.Schema

    schema "users" do
      field :avatar, R2B2.File, load: :download
    end
  end
  ```

  `db`: Does not make a call to B2 and will return the information stored in the database.
  `download`: Downloads the file and stored in `file`
  `info`: Default. Will grab the file info from B2.
  `link`: Grabs a link that can be used to download the file.
  """

  use Ecto.ParameterizedType

  alias R2B2.API.RemoteFile

  @impl true
  def type(_opts), do: :string

  @impl true
  def init(opts), do: opts

  @impl true
  def cast(%RemoteFile{} = file, _opts), do: {:ok, file}
  def cast(%{} = map, _opts), do: {:ok, struct(RemoteFile, map)}
  def cast(nil, opts), do: cast(%{}, opts)
  def cast(_, _), do: :error

  @impl true
  def load(nil, loader, opts), do: load(%{}, loader, opts)

  def load(data, _loader, opts) do
    file = struct(RemoteFile, R2B2.Util.atomize_keys(data))

    RemoteFile
    |> struct(R2B2.Util.atomize_keys(data))
    |> do_load(Keyword.get(opts, :client), Keyword.get(opts, :load, :info))
    |> case do
      {:ok, _} = result -> result
      {:error, _} -> {:ok, file}
    end
  end

  def do_load(%{id: nil} = file, _client, _), do: {:ok, file}
  def do_load(file, _client, :db), do: {:ok, file}
  def do_load(file, client, :download), do: R2B2.Client.download(client, file)
  def do_load(file, client, :info), do: R2B2.Client.info(client, file)
  def do_load(file, client, :link), do: R2B2.Client.download_link(client, file)

  @impl true
  def dump(%RemoteFile{} = file, _dumper, opts) do
    fields =
      (~w|id file_name|a ++ Keyword.get(opts, :fields, []))
      |> Enum.reject(&(&1 == :file))

    {map, _} = Map.split(file, fields)

    map =
      for {key, val} <- map,
          into: %{},
          do: {Atom.to_string(key), val}

    {:ok, map}
  end

  def dump(_, _), do: :error
end
