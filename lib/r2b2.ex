defmodule R2B2 do
  @moduledoc """
  Documentation for `R2-B2`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> R2B2.hello()
      :world

  """
  def hello do
    :world
  end
end
